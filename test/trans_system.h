#ifndef TEST_TRANS_SYSTEM_H
#define TEST_TRANS_SYSTEM_H

TEST(testTransSystemSetUp);
TEST(testTransSystemInit_test);
TEST(testTransSystemMerge_test);
TEST(testTransSystemInit_test_depot1);
TEST(testTransSystemMerge_test_depot1);

#define P(N, P) \
    TEST(testTransSystemInit_##N); \
    TEST(testTransSystemMerge_##N);
#include "trans_system_prob.h"

TEST_SUITE(TSTransSystem) {
    TEST_ADD(testTransSystemSetUp),
    TEST_ADD(testTransSystemInit_test),
    TEST_ADD(testTransSystemMerge_test),
    TEST_ADD(testTransSystemInit_test_depot1),
    TEST_ADD(testTransSystemMerge_test_depot1),
#define P(N, P) \
    TEST_ADD(testTransSystemInit_##N), \
    TEST_ADD(testTransSystemMerge_##N),
#include "trans_system_prob.h"
    TEST_SUITE_CLOSURE
};
#endif


