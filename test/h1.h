#ifndef TEST_H1_H
#define TEST_H1_H

TEST(testH1SetUp);

#define P(N, P) \
    TEST(testH1_##N);
#define NCE(N, P) \
    TEST(testH1_##N##_noce_strips);
#include "h1_prob.h"

TEST_SUITE(TSH1) {
    TEST_ADD(testH1SetUp),
#define P(N, P) \
    TEST_ADD(testH1_##N),
#define NCE(N, P) \
    TEST_ADD(testH1_##N##_noce_strips),
#include "h1_prob.h"
    TEST_SUITE_CLOSURE
};
#endif
