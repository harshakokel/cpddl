#include <sys/time.h>
#include <sys/resource.h>
#include <cu/cu.h>
#include "pddl/pddl.h"

static void setMemLimit(void)
{
    struct rlimit mem_limit;

    mem_limit.rlim_cur = mem_limit.rlim_max = 512 * 1024 * 1024;
    mem_limit.rlim_cur = mem_limit.rlim_max = 2024 * 1024 * 1024;
    setrlimit(RLIMIT_AS, &mem_limit);
}

TEST(testTNFSetUp)
{
    setMemLimit();
}


static int flowEq(const pddl_fdr_t *fdr,
                  const pddl_fdr_t *tnf,
                  const pddl_plan_file_fdr_t *plan)
{
    pddl_hflow_t fdr_flow;
    pddlHFlowInit(&fdr_flow, fdr, 0);

    pddl_hflow_t tnf_flow;
    pddlHFlowInit(&tnf_flow, tnf, 0);

    int fdr_cost = pddlHFlow(&fdr_flow, fdr->init, NULL);
    int tnf_cost = pddlHFlow(&tnf_flow, tnf->init, NULL);
    assertEquals(fdr_cost, tnf_cost);
    if (fdr_cost != tnf_cost)
        fprintf(stderr, "Flow failed. fdr: %d tnf: %d\n", fdr_cost, tnf_cost);

    pddlHFlowFree(&fdr_flow);
    pddlHFlowFree(&tnf_flow);
    return 0;
}

static int potEq(const pddl_fdr_t *fdr,
                 const pddl_fdr_t *tnf,
                 const pddl_plan_file_fdr_t *plan)
{
    /*
    FILE *fout = fopen("a", "w");
    pddlFDRPrintFD(fdr, NULL, fout);
    fclose(fout);
    fout = fopen("b", "w");
    pddlFDRPrintFD(tnf, NULL, fout);
    fclose(fout);
    */

    pddl_pot_t hfdr, htnf;
    pddlPotInitFDR(&hfdr, fdr);
    pddlPotInitFDR(&htnf, tnf);

    pddlPotSetObjFDRState(&hfdr, &fdr->var, fdr->init);
    pddlPotSetObjFDRState(&htnf, &tnf->var, tnf->init);

    size_t w_size = htnf.var_size;
    if (hfdr.var_size > w_size)
        w_size = hfdr.var_size;
    double *w = BOR_ALLOC_ARR(double, w_size);

    int ret = pddlPotSolve(&hfdr, w, hfdr.var_size, 0);
    assertEquals(ret, 0);
    double fdr_c = 0.;
    for (int i = 0; i < fdr->var.var_size; ++i)
        fdr_c += w[fdr->var.var[i].val[fdr->init[i]].global_id];

    ret = pddlPotSolve(&htnf, w, htnf.var_size, 0);
    assertEquals(ret, 0);
    double tnf_c = 0.;
    for (int i = 0; i < tnf->var.var_size; ++i)
        tnf_c += w[tnf->var.var[i].val[tnf->init[i]].global_id];

    assertTrue(fabs(fdr_c - tnf_c) < 0.1);
    if (!(fabs(fdr_c - tnf_c) < 0.1))
        fprintf(stderr, "Pot failed. fdr: %.6f tnf: %.6f\n", fdr_c, tnf_c);

    BOR_FREE(w);
    pddlPotFree(&hfdr);
    pddlPotFree(&htnf);
    return 0;
}

static int heurEq(const pddl_fdr_t *fdr,
                  const pddl_fdr_t *tnf,
                  const pddl_plan_file_fdr_t *plan)
{
    return flowEq(fdr, tnf, plan) | potEq(fdr, tnf, plan);
}

static int flowLT(const pddl_fdr_t *fdr,
                  const pddl_fdr_t *tnf,
                  const pddl_plan_file_fdr_t *plan)
{
    pddl_hflow_t fdr_flow;
    pddlHFlowInit(&fdr_flow, fdr, 0);

    pddl_hflow_t tnf_flow;
    pddlHFlowInit(&tnf_flow, tnf, 0);

    int fdr_cost = pddlHFlow(&fdr_flow, fdr->init, NULL);
    int tnf_cost = pddlHFlow(&tnf_flow, tnf->init, NULL);
    assertTrue(fdr_cost <= tnf_cost);
    if (fdr_cost > tnf_cost)
        fprintf(stderr, "Flow failed. fdr: %d tnf: %d\n", fdr_cost, tnf_cost);

    pddlHFlowFree(&fdr_flow);
    pddlHFlowFree(&tnf_flow);
    return 0;
}

static int potLT(const pddl_fdr_t *fdr,
                 const pddl_fdr_t *tnf,
                 const pddl_plan_file_fdr_t *plan)
{
    /*
    FILE *fout = fopen("a", "w");
    pddlFDRPrintFD(fdr, NULL, fout);
    fclose(fout);
    fout = fopen("b", "w");
    pddlFDRPrintFD(tnf, NULL, fout);
    fclose(fout);
    */

    pddl_pot_t hfdr, htnf;
    pddlPotInitFDR(&hfdr, fdr);
    pddlPotInitFDR(&htnf, tnf);

    pddlPotSetObjFDRState(&hfdr, &fdr->var, fdr->init);
    pddlPotSetObjFDRState(&htnf, &tnf->var, tnf->init);

    size_t w_size = htnf.var_size;
    if (hfdr.var_size > w_size)
        w_size = hfdr.var_size;
    double *w = BOR_ALLOC_ARR(double, w_size);

    int ret = pddlPotSolve(&hfdr, w, hfdr.var_size, 0);
    assertEquals(ret, 0);
    double fdr_c = 0.;
    for (int i = 0; i < fdr->var.var_size; ++i)
        fdr_c += w[fdr->var.var[i].val[fdr->init[i]].global_id];

    ret = pddlPotSolve(&htnf, w, htnf.var_size, 0);
    assertEquals(ret, 0);
    double tnf_c = 0.;
    for (int i = 0; i < tnf->var.var_size; ++i)
        tnf_c += w[tnf->var.var[i].val[tnf->init[i]].global_id];

    assertTrue(tnf_c - fdr_c > -0.1);
    if (tnf_c - fdr_c <= -0.1)
        fprintf(stderr, "Pot failed. fdr: %.6f tnf: %.6f\n", fdr_c, tnf_c);

    BOR_FREE(w);
    pddlPotFree(&hfdr);
    pddlPotFree(&htnf);
    return 0;
}

static int heurLT(const pddl_fdr_t *fdr,
                  const pddl_fdr_t *tnf,
                  const pddl_plan_file_fdr_t *plan)
{
    return flowLT(fdr, tnf, plan) | potLT(fdr, tnf, plan);
}


static int test(const char *fn,
                const char *plan_fn,
                int (*cb)(const pddl_fdr_t *fdr,
                          const pddl_fdr_t *tnf,
                          const pddl_plan_file_fdr_t *plan),
                int dis)
{
    bor_err_t err = BOR_ERR_INIT;
    borErrWarnEnable(&err, NULL);
    borErrInfoEnable(&err, NULL);

    pddl_files_t files;
    if (pddlFiles1(&files, fn, &err) != 0){
        borErrPrint(&err, 1, stderr);
        return -1;
    }

    // Parse PDDL
    pddl_config_t pddl_cfg = PDDL_CONFIG_INIT;
    pddl_cfg.force_adl = 1;
    pddl_t pddl;
    if (pddlInit(&pddl, files.domain_pddl, files.problem_pddl,
                 &pddl_cfg, &err) != 0){
        fprintf(stderr, "Error: ");
        borErrPrint(&err, 1, stderr);
        return -1;
    }
    pddlNormalize(&pddl);
    pddlCheckSizeTypes(&pddl);

    // Lifted mgroups
    pddl_lifted_mgroups_infer_limits_t lifted_mgroups_limits
                = PDDL_LIFTED_MGROUPS_INFER_LIMITS_INIT;
    pddl_lifted_mgroups_t lifted_mgroups;
    pddlLiftedMGroupsInit(&lifted_mgroups);
    pddlLiftedMGroupsInferFAMGroups(&pddl, &lifted_mgroups_limits,
                                    &lifted_mgroups, &err);
    pddlLiftedMGroupsSetExactlyOne(&pddl, &lifted_mgroups, &err);
    pddlLiftedMGroupsSetStatic(&pddl, &lifted_mgroups, &err);

    // Ground to STRIPS
    pddl_ground_config_t ground_cfg = PDDL_GROUND_CONFIG_INIT;
    ground_cfg.lifted_mgroups = &lifted_mgroups;
    ground_cfg.prune_op_pre_mutex = 1;
    ground_cfg.prune_op_dead_end = 1;
    pddl_strips_t strips;
    if (pddlStripsGround(&strips, &pddl, &ground_cfg, &err) != 0){
        BOR_INFO2(&err, "Grounding failed.");
        fprintf(stderr, "Error: ");
        borErrPrint(&err, 1, stderr);
        return -1;
    }
    if (strips.has_cond_eff){
        BOR_INFO2(&err, "Has conditional effects -- terminating...");
        return -1;
    }

    // Ground mutex groups
    pddl_mgroups_t mgroups;
    pddlMGroupsGround(&mgroups, &pddl, &lifted_mgroups, &strips);
    pddlMGroupsSetExactlyOne(&mgroups, &strips);
    pddlMGroupsSetGoal(&mgroups, &strips);

    // Prune strips
    if (!strips.has_cond_eff){
        BOR_ISET(rm_fact);
        BOR_ISET(rm_op);
        if (pddlIrrelevanceAnalysis(&strips, &rm_fact, &rm_op, NULL, &err) != 0){
            BOR_INFO2(&err, "Irrelevance analysis failed.");
            fprintf(stderr, "Error: ");
            borErrPrint(&err, 1, stderr);
            return -1;
        }
        if (borISetSize(&rm_fact) > 0 || borISetSize(&rm_op) > 0){
            pddlStripsReduce(&strips, &rm_fact, &rm_op);
            if (borISetSize(&rm_fact) > 0)
                pddlMGroupsReduce(&mgroups, &rm_fact);
        }
        borISetFree(&rm_fact);
        borISetFree(&rm_op);
    }

    // Construct FDR
    pddl_fdr_t fdr;
    unsigned fdr_var_flag = PDDL_FDR_VARS_LARGEST_FIRST;
    unsigned fdr_flag = 0;
    pddl_mutex_pairs_t mutex;
    pddlMutexPairsInitStrips(&mutex, &strips);
    pddlMutexPairsAddMGroups(&mutex, &mgroups);
    pddlFDRInitFromStrips(&fdr, &strips, &mgroups, &mutex, fdr_var_flag,
                          fdr_flag, &err);
    if (pddlPruneFDR(&fdr, &err) != 0){
        BOR_INFO2(&err, "Pruning failed.");
        fprintf(stderr, "Error: ");
        borErrPrint(&err, 1, stderr);
        return -1;
    }

    // Find mutexes
    if (dis){
        pddl_mg_strips_t mg_strips;
        pddlMGStripsInitFDR(&mg_strips, &fdr);
        pddlMutexPairsFree(&mutex);
        pddlMutexPairsInitStrips(&mutex, &mg_strips.strips);
        pddlMutexPairsAddMGroups(&mutex, &mg_strips.mg);
        pddlH2(&mg_strips.strips, &mutex, NULL, NULL, 0., &err);
        pddlMGStripsFree(&mg_strips);
    }

    // Construct TNF
    pddl_fdr_t tnf;
    if (dis){
        pddlFDRInitTransitionNormalForm(&tnf, &fdr, &mutex, 0, &err);
    }else{
        pddlFDRInitTransitionNormalForm(&tnf, &fdr, NULL, 0, &err);
    }

    pddl_plan_file_fdr_t plan;
    int ret = pddlPlanFileFDRInit(&plan, &fdr, plan_fn, &err);
    assertEquals(ret, 0);
    if (ret == 0){
        cb(&fdr, &tnf, &plan);
        pddlPlanFileFDRFree(&plan);
    }else{
        fprintf(stderr, "Error: ");
        borErrPrint(&err, 1, stderr);
    }

    pddlFDRFree(&tnf);
    pddlFDRFree(&fdr);
    pddlMutexPairsFree(&mutex);
    pddlMGroupsFree(&mgroups);
    pddlStripsFree(&strips);
    pddlLiftedMGroupsFree(&lifted_mgroups);
    pddlFree(&pddl);
    return 0;
}

#define P(N, P) \
TEST(testTNFHeurEq_##N) \
{ \
    test("pddl-data/ipc-opt-noce/" P, \
         "pddl-data/ipc-opt-noce/" P ".plan", \
         heurEq, 0); \
} \
TEST(testDTNFHeurLT_##N) \
{ \
    test("pddl-data/ipc-opt-noce/" P, \
         "pddl-data/ipc-opt-noce/" P ".plan", \
         heurLT, 1); \
}
#include "tnf_prob.h"
