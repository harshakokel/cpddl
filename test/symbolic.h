#ifndef TEST_SYMBOLIC_H
#define TEST_SYMBOLIC_H

TEST(testSymbolicSetUp);
TEST(testSymbolic);

#define P(N, P) \
    TEST(testSymbolicFwBw_##N);
#define PFW(N, P) \
    TEST(testSymbolicFw_##N);
#define PBW(N, P) \
    TEST(testSymbolicBw_##N);
#include "symbolic_prob.h"

TEST_SUITE(TSSymbolic){
    TEST_ADD(testSymbolicSetUp),
    TEST_ADD(testSymbolic),
#define P(N, P) \
    TEST_ADD(testSymbolicFwBw_##N),
#define PFW(N, P) \
    TEST_ADD(testSymbolicFw_##N),
#define PBW(N, P) \
    TEST_ADD(testSymbolicBw_##N),
#include "symbolic_prob.h"
    TEST_SUITE_CLOSURE
};

#endif
