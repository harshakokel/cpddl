#include <sys/time.h>
#include <sys/resource.h>
#include <cu/cu.h>
#include "pddl/pddl.h"
#include "pddl/symbolic_task.h"

static void setMemLimit(void)
{
    struct rlimit mem_limit;

    //mem_limit.rlim_cur = mem_limit.rlim_max = 512ul * 1024ul * 1024ul;
    mem_limit.rlim_cur = mem_limit.rlim_max = 4096ul * 1024ul * 1024ul;
    setrlimit(RLIMIT_AS, &mem_limit);
}

TEST(testSymbolicSetUp)
{
    setMemLimit();
}

static void checkPlanStates(const char *fn,
                            const pddl_fdr_t *fdr,
                            pddl_symbolic_task_t *ss)
{
    pddl_plan_file_fdr_t planf;
    char plan_fn[256];
    sprintf(plan_fn, "%s.plan", fn);
    int res = pddlPlanFileFDRInit(&planf, fdr, plan_fn, NULL);
    assertTrue(res == 0);

    for (int si = 0; si < planf.state_size - 1; ++si){
        int op_id = borIArrGet(&planf.op, si);
        assertTrue(pddlSymbolicTaskCheckApplyFw(ss, planf.state[si],
                                                planf.state[si + 1], op_id));
    }

    for (int si = planf.state_size - 1; si > 0; --si){
        int op_id = borIArrGet(&planf.op, si - 1);
        assertTrue(pddlSymbolicTaskCheckApplyBw(ss, planf.state[si],
                                                planf.state[si - 1], op_id));
    }

    assertTrue(pddlSymbolicTaskCheckPlan(ss, &planf.op, planf.state_size - 1));

    pddlPlanFileFDRFree(&planf);
}

static void checkPlan(const char *fn,
                      const pddl_strips_t *strips,
                      const bor_iarr_t *plan)
{
    BOR_ISET(state);
    borISetUnion(&state, &strips->init);
    int plan_cost = 0;
    int op_id;
    BOR_IARR_FOR_EACH(plan, op_id){
        const pddl_strips_op_t *op = strips->op.op[op_id];
        plan_cost += op->cost;
        assertTrue(borISetIsSubset(&op->pre, &state));
        if (!borISetIsSubset(&op->pre, &state)){
            fprintf(stderr, "Failed on operator %d\n", op_id);
            return;
        }
        BOR_ISET(state2);
        borISetMinus2(&state2, &state, &op->del_eff);
        borISetUnion(&state2, &op->add_eff);
        for (int cei = 0; cei < op->cond_eff_size; ++cei){
            const pddl_strips_op_cond_eff_t *ce = &op->cond_eff[cei];
            if (borISetIsSubset(&ce->pre, &state)){
                borISetMinus(&state2, &ce->del_eff);
                borISetUnion(&state2, &ce->add_eff);
            }
        }
        borISetEmpty(&state);
        borISetUnion(&state, &state2);
        borISetFree(&state2);
    }
    assertTrue(borISetIsSubset(&strips->goal, &state));
    borISetFree(&state);

    pddl_plan_file_strips_t planf;
    char plan_fn[256];
    sprintf(plan_fn, "%s.plan", fn);
    int res = pddlPlanFileStripsInit(&planf, strips, plan_fn, NULL);
    assertTrue(res == 0);
    assertEquals(planf.cost, plan_cost);
    pddlPlanFileStripsFree(&planf);
}

static void plan(const char *fn,
                 const pddl_strips_t *strips,
                 pddl_symbolic_task_t *task,
                 int (*symb_plan)(pddl_symbolic_task_t *,
                                  bor_iarr_t *,
                                  bor_err_t *),
                 bor_err_t *err)
{
    BOR_IARR(plan);
    int res = symb_plan(task, &plan, err);
    assertTrue(res == PDDL_SYMBOLIC_PLAN_FOUND);
    if (res == PDDL_SYMBOLIC_PLAN_FOUND)
        checkPlan(fn, strips, &plan);
    borIArrFree(&plan);
}

static int test(const char *fn, int (*symb_plan)(pddl_symbolic_task_t *,
                                                 bor_iarr_t *,
                                                 bor_err_t *))
{
    bor_err_t err = BOR_ERR_INIT;
    borErrWarnEnable(&err, NULL);
    borErrInfoEnable(&err, NULL);
    //borErrInfoEnable(&err, stdout);

    pddl_files_t files;
    if (pddlFiles1(&files, fn, &err) != 0){
        borErrPrint(&err, 1, stderr);
        return -1;
    }

    // Parse PDDL
    pddl_config_t pddl_cfg = PDDL_CONFIG_INIT;
    pddl_cfg.force_adl = 1;
    pddl_t pddl;
    if (pddlInit(&pddl, files.domain_pddl, files.problem_pddl,
                 &pddl_cfg, &err) != 0){
        fprintf(stderr, "Error: ");
        borErrPrint(&err, 1, stderr);
        return -1;
    }
    pddlNormalize(&pddl);
    pddlCheckSizeTypes(&pddl);

    // Lifted mgroups
    pddl_lifted_mgroups_infer_limits_t lifted_mgroups_limits
                = PDDL_LIFTED_MGROUPS_INFER_LIMITS_INIT;
    pddl_lifted_mgroups_t lifted_mgroups;
    pddlLiftedMGroupsInit(&lifted_mgroups);
    pddlLiftedMGroupsInferFAMGroups(&pddl, &lifted_mgroups_limits,
                                    &lifted_mgroups, &err);
    pddlLiftedMGroupsSetExactlyOne(&pddl, &lifted_mgroups, &err);
    pddlLiftedMGroupsSetStatic(&pddl, &lifted_mgroups, &err);

    // Ground to STRIPS
    pddl_ground_config_t ground_cfg = PDDL_GROUND_CONFIG_INIT;
    ground_cfg.lifted_mgroups = &lifted_mgroups;
    ground_cfg.prune_op_pre_mutex = 1;
    ground_cfg.prune_op_dead_end = 1;
    pddl_strips_t strips;
    if (pddlStripsGround(&strips, &pddl, &ground_cfg, &err) != 0){
        BOR_INFO2(&err, "Grounding failed.");
        fprintf(stderr, "Error: ");
        borErrPrint(&err, 1, stderr);
        return -1;
    }
    if (strips.has_cond_eff){
        BOR_INFO2(&err, "Has conditional effects -- terminating...");
        return -1;
    }

    // Prune strips
    if (!strips.has_cond_eff){
        BOR_ISET(rm_fact);
        BOR_ISET(rm_op);

        pddl_mutex_pairs_t mutex;
        pddlMutexPairsInitStrips(&mutex, &strips);
        pddlH2(&strips, &mutex, &rm_fact, &rm_op, 0., &err);
        if (borISetSize(&rm_fact) > 0 || borISetSize(&rm_op) > 0)
            pddlStripsReduce(&strips, &rm_fact, &rm_op);
        pddlMutexPairsFree(&mutex);

        borISetEmpty(&rm_fact);
        borISetEmpty(&rm_op);

        if (pddlIrrelevanceAnalysis(&strips, &rm_fact, &rm_op, NULL, &err) != 0){
            BOR_INFO2(&err, "Irrelevance analysis failed.");
            fprintf(stderr, "Error: ");
            borErrPrint(&err, 1, stderr);
            return -1;
        }
        if (borISetSize(&rm_fact) > 0 || borISetSize(&rm_op) > 0)
            pddlStripsReduce(&strips, &rm_fact, &rm_op);
        borISetFree(&rm_fact);
        borISetFree(&rm_op);
    }

    // Ground mutex groups
    pddl_mgroups_t mgroups;
    pddlMGroupsGround(&mgroups, &pddl, &lifted_mgroups, &strips);
    pddlMGroupsSetExactlyOne(&mgroups, &strips);
    pddlMGroupsSetGoal(&mgroups, &strips);

    pddl_mutex_pairs_t mutex;
    pddlMutexPairsInitStrips(&mutex, &strips);
    //pddlH2(&mg_strips.strips, &mutex, NULL, NULL, 0., &err);
    pddlH2FwBw(&strips, &mgroups, &mutex, NULL, NULL, 0., &err);
    pddlMutexPairsAddMGroups(&mutex, &mgroups);

    pddl_fdr_t fdr;
    unsigned fdr_var_flag = PDDL_FDR_VARS_ESSENTIAL_FIRST;
    pddlFDRInitFromStrips(&fdr, &strips, &mgroups, &mutex,
                         fdr_var_flag, 0, &err);

    //pddlStripsPrintDebug(&strips, stdout);

    pddl_symbolic_task_config_t symb_cfg = PDDL_SYMBOLIC_TASK_CONFIG_INIT;
    //symb_cfg.use_constr = 1;
    //symb_cfg.use_op_constr = 0;

    pddl_symbolic_task_t *st;

    st = pddlSymbolicTaskNew(&fdr, &symb_cfg, &err);
    if (symb_plan == pddlSymbolicTaskSearchFwBw)
        checkPlanStates(fn, &fdr, st);

    if (symb_plan != NULL){
        plan(fn, &strips, st, symb_plan, &err);
    }else{
        plan(fn, &strips, st, pddlSymbolicTaskSearchFw, &err);
        plan(fn, &strips, st, pddlSymbolicTaskSearchBw, &err);
        plan(fn, &strips, st, pddlSymbolicTaskSearchFwBw, &err);
    }

    pddlSymbolicTaskDel(st);

    pddlFDRFree(&fdr);
    pddlMutexPairsFree(&mutex);
    pddlMGroupsFree(&mgroups);
    pddlStripsFree(&strips);
    pddlLiftedMGroupsFree(&lifted_mgroups);
    pddlFree(&pddl);
    return 0;
}

TEST(testSymbolic)
{
    test("pddl-data/test-seq/test/pfile", NULL);
    //test("pddl-data/ipc-2014/seq-opt/cavediving/testing05A_easy", NULL);
    //test("pddl-data/ipc-opt-noce/openstacks08/p01", NULL);
    //test("pddl-data/ipc-opt-noce/agricola18/p01", NULL);
}

#define P(N, P) \
TEST(testSymbolicFwBw_##N) \
{ \
    test("pddl-data/ipc-opt-noce/" P, pddlSymbolicTaskSearchFwBw); \
}

#define PFW(N, P) \
TEST(testSymbolicFw_##N) \
{ \
    test("pddl-data/ipc-opt-noce/" P, pddlSymbolicTaskSearchFw); \
}\

#define PBW(N, P) \
TEST(testSymbolicBw_##N) \
{ \
    test("pddl-data/ipc-opt-noce/" P, pddlSymbolicTaskSearchBw); \
}
#include "symbolic_prob.h"
