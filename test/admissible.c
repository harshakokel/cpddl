#include <sys/time.h>
#include <sys/resource.h>
#include <cu/cu.h>
#include "pddl/pddl.h"

static void setMemLimit(void)
{
    struct rlimit mem_limit;

    mem_limit.rlim_cur = mem_limit.rlim_max = 512 * 1024 * 1024;
    mem_limit.rlim_cur = mem_limit.rlim_max = 2024 * 1024 * 1024;
    setrlimit(RLIMIT_AS, &mem_limit);
}

TEST(testAdmissibleSetUp)
{
    setMemLimit();
}

static int lmCut(const pddl_fdr_t *fdr,
                 const pddl_plan_file_fdr_t *plan)
{
    pddl_lm_cut_t lmc;
    pddlLMCutInit(&lmc, fdr, 0, 0);
    int cost = plan->cost;
    for (int i = 0; i < plan->state_size; ++i){
        int c = pddlLMCut(&lmc, plan->state[i], NULL, NULL);
        if (c > cost){
            fprintf(stderr, "!!!ERR %d\n", i);
            return -1;
        }
        assertTrue(c <= cost);
        if (i < borIArrSize(&plan->op))
            cost -= fdr->op.op[borIArrGet(&plan->op, i)]->cost;
    }
    pddlLMCutFree(&lmc);

    return 0;
}

static int flow(const pddl_fdr_t *fdr,
                const pddl_plan_file_fdr_t *plan)
{
    pddl_hflow_t flw;
    pddlHFlowInit(&flw, fdr, 0);
    int cost = plan->cost;
    for (int i = 0; i < plan->state_size; ++i){
        int c = pddlHFlow(&flw, plan->state[i], NULL);
        if (c > cost){
            fprintf(stderr, "!!!ERR %d\n", i);
            return -1;
        }
        assertTrue(c <= cost);
        if (i < borIArrSize(&plan->op))
            cost -= fdr->op.op[borIArrGet(&plan->op, i)]->cost;
    }
    pddlHFlowFree(&flw);

    return 0;
}

static int test(const char *fn,
                const char *plan_fn,
                int (*cb)(const pddl_fdr_t *fdr,
                          const pddl_plan_file_fdr_t *plan))
{
    bor_err_t err = BOR_ERR_INIT;
    borErrWarnEnable(&err, NULL);
    borErrInfoEnable(&err, NULL);

    pddl_files_t files;
    if (pddlFiles1(&files, fn, &err) != 0){
        borErrPrint(&err, 1, stderr);
        return -1;
    }

    // Parse PDDL
    pddl_config_t pddl_cfg = PDDL_CONFIG_INIT;
    pddl_cfg.force_adl = 1;
    pddl_t pddl;
    if (pddlInit(&pddl, files.domain_pddl, files.problem_pddl,
                 &pddl_cfg, &err) != 0){
        fprintf(stderr, "Error: ");
        borErrPrint(&err, 1, stderr);
        return -1;
    }
    pddlNormalize(&pddl);
    pddlCheckSizeTypes(&pddl);

    // Lifted mgroups
    pddl_lifted_mgroups_infer_limits_t lifted_mgroups_limits
                = PDDL_LIFTED_MGROUPS_INFER_LIMITS_INIT;
    pddl_lifted_mgroups_t lifted_mgroups;
    pddlLiftedMGroupsInit(&lifted_mgroups);
    pddlLiftedMGroupsInferFAMGroups(&pddl, &lifted_mgroups_limits,
                                    &lifted_mgroups, &err);
    pddlLiftedMGroupsSetExactlyOne(&pddl, &lifted_mgroups, &err);
    pddlLiftedMGroupsSetStatic(&pddl, &lifted_mgroups, &err);

    // Ground to STRIPS
    pddl_ground_config_t ground_cfg = PDDL_GROUND_CONFIG_INIT;
    ground_cfg.lifted_mgroups = &lifted_mgroups;
    ground_cfg.prune_op_pre_mutex = 1;
    ground_cfg.prune_op_dead_end = 1;
    pddl_strips_t strips;
    if (pddlStripsGround(&strips, &pddl, &ground_cfg, &err) != 0){
        BOR_INFO2(&err, "Grounding failed.");
        fprintf(stderr, "Error: ");
        borErrPrint(&err, 1, stderr);
        return -1;
    }
    if (strips.has_cond_eff){
        BOR_INFO2(&err, "Has conditional effects -- terminating...");
        return -1;
    }

    // Ground mutex groups
    pddl_mgroups_t mgroups;
    pddlMGroupsGround(&mgroups, &pddl, &lifted_mgroups, &strips);
    pddlMGroupsSetExactlyOne(&mgroups, &strips);
    pddlMGroupsSetGoal(&mgroups, &strips);

    // Prune strips
    if (!strips.has_cond_eff){
        BOR_ISET(rm_fact);
        BOR_ISET(rm_op);
        if (pddlIrrelevanceAnalysis(&strips, &rm_fact, &rm_op, NULL, &err) != 0){
            BOR_INFO2(&err, "Irrelevance analysis failed.");
            fprintf(stderr, "Error: ");
            borErrPrint(&err, 1, stderr);
            return -1;
        }
        if (borISetSize(&rm_fact) > 0 || borISetSize(&rm_op) > 0){
            pddlStripsReduce(&strips, &rm_fact, &rm_op);
            if (borISetSize(&rm_fact) > 0)
                pddlMGroupsReduce(&mgroups, &rm_fact);
        }
        borISetFree(&rm_fact);
        borISetFree(&rm_op);
    }

    // Construct FDR
    pddl_fdr_t fdr;
    unsigned fdr_var_flag = PDDL_FDR_VARS_LARGEST_FIRST;
    unsigned fdr_flag = 0;
    pddl_mutex_pairs_t mutex;
    pddlMutexPairsInitStrips(&mutex, &strips);
    pddlMutexPairsAddMGroups(&mutex, &mgroups);
    pddlFDRInitFromStrips(&fdr, &strips, &mgroups, &mutex, fdr_var_flag,
                          fdr_flag, &err);
    if (pddlPruneFDR(&fdr, &err) != 0){
        BOR_INFO2(&err, "Pruning failed.");
        fprintf(stderr, "Error: ");
        borErrPrint(&err, 1, stderr);
        return -1;
    }

    pddl_plan_file_fdr_t plan;
    int ret = pddlPlanFileFDRInit(&plan, &fdr, plan_fn, &err);
    assertEquals(ret, 0);
    if (ret == 0){
        cb(&fdr, &plan);
        pddlPlanFileFDRFree(&plan);
    }else{
        fprintf(stderr, "Error: ");
        borErrPrint(&err, 1, stderr);
    }

    pddlFDRFree(&fdr);
    pddlMutexPairsFree(&mutex);
    pddlMGroupsFree(&mgroups);
    pddlStripsFree(&strips);
    pddlLiftedMGroupsFree(&lifted_mgroups);
    pddlFree(&pddl);
    return 0;
}

#define P(N, P) \
TEST(testAdmissibleLMCut_##N) \
{ \
    test("pddl-data/ipc-opt-noce/" P, \
         "pddl-data/ipc-opt-noce/" P ".plan", \
         lmCut); \
} \
TEST(testAdmissibleFlow_##N) \
{ \
    test("pddl-data/ipc-opt-noce/" P, \
         "pddl-data/ipc-opt-noce/" P ".plan", \
         flow); \
}
#include "admissible_prob.h"
