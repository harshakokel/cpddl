#ifndef TEST_HFF_H
#define TEST_HFF_H

TEST(testHFFSetUp);

#define P(N, P) \
    TEST(testHFF_##N);
#define NCE(N, P) \
    TEST(testHFF_##N##_noce_strips);
#include "hff_prob.h"

TEST_SUITE(TSHFF) {
    TEST_ADD(testHFFSetUp),
#define P(N, P) \
    TEST_ADD(testHFF_##N),
#define NCE(N, P) \
    TEST_ADD(testHFF_##N##_noce_strips),
#include "hff_prob.h"
    TEST_SUITE_CLOSURE
};
#endif
