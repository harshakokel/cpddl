#include <sys/time.h>
#include <sys/resource.h>
#include <cu/cu.h>
#include "pddl/pddl.h"

static void run(const char *domain_fn, const char *problem_fn,
                const char *outfn,
                int compile_away_cond_eff,
                int compile_away_cond_eff_strips)
{
    pddl_config_t cfg = PDDL_CONFIG_INIT;
    pddl_ground_config_t ground_cfg = PDDL_GROUND_CONFIG_INIT;
    pddl_t pddl;
    bor_err_t err = BOR_ERR_INIT;
    pddl_strips_t strips;
    int ret;

    cfg.force_adl = 1;
    ret = pddlInit(&pddl, domain_fn, problem_fn, &cfg, &err);
    assertEquals(ret, 0);
    if (ret != 0){
        borErrPrint(&err, 1, stderr);
        return;
    }
    pddlCheckSizeTypes(&pddl);

    pddlNormalize(&pddl);
    if (compile_away_cond_eff)
        pddlCompileAwayNonStaticCondEff(&pddl);

    ret = pddlStripsGround(&strips, &pddl, &ground_cfg, &err);
    assertEquals(ret, 0);
    if (compile_away_cond_eff_strips)
        pddlStripsCompileAwayCondEff(&strips);

    if (borISetSize(&strips.init) > 0)
        strips.init.size = BOR_MAX(1, strips.init.size - 1);

    BOR_ISET(unreachable_fact);
    BOR_ISET(unreachable_op);

    //borErrInfoEnable(&err, stdout);
    ret = pddlH1(&strips, &unreachable_fact, &unreachable_op, &err);
    //pddlStripsPrintDebug(&strips, stdout);

    if (strips.has_cond_eff){
        assertEquals(ret, -1);
        borErrPrint(&err, 0, stdout);
        goto end;
    }else{
        assertEquals(ret, 0);

        if (borISetSize(&unreachable_fact) > 0
                || borISetSize(&unreachable_op) > 0){
            fprintf(stdout, "Init:");
            int fact;
            BOR_ISET_FOR_EACH(&strips.init, fact)
                fprintf(stdout, " (%s)", strips.fact.fact[fact]->name);
            fprintf(stdout, "\n");
        }

        if (borISetSize(&unreachable_fact) > 0){
            fprintf(stdout, "Unreachable facts [%d/%d]:\n",
                    borISetSize(&unreachable_fact), strips.fact.fact_size);
            int fact;
            BOR_ISET_FOR_EACH(&unreachable_fact, fact){
                fprintf(stdout, "  (%s)\n", strips.fact.fact[fact]->name);
            }
        }
        if (borISetSize(&unreachable_op) > 0){
            fprintf(stdout, "Unreachable ops [%d/%d]:\n",
                    borISetSize(&unreachable_op), strips.op.op_size);
            int op;
            BOR_ISET_FOR_EACH(&unreachable_op, op)
                fprintf(stdout, "  (%s)\n", strips.op.op[op]->name);
        }
    }

end:
    borISetFree(&unreachable_fact);
    borISetFree(&unreachable_op);

    pddlFree(&pddl);
    pddlStripsFree(&strips);
}

static void setMemLimit(void)
{
    struct rlimit mem_limit;

    mem_limit.rlim_cur = mem_limit.rlim_max = 512 * 1024 * 1024;
    mem_limit.rlim_cur = mem_limit.rlim_max = 2024 * 1024 * 1024;
    setrlimit(RLIMIT_AS, &mem_limit);
}

TEST(testH1SetUp)
{
    setMemLimit();
}

#define P(N, P) \
TEST(testH1_##N) \
{ \
    pddl_files_t files; \
    bor_err_t err = BOR_ERR_INIT; \
    if (pddlFiles(&files, "pddl-data/", P, &err) != 0){ \
        borErrPrint(&err, 1, stderr); \
        return; \
    } \
    run(files.domain_pddl, files.problem_pddl, #N, 0, 0); \
}

#define NCE(N, P) \
TEST(testH1_##N##_noce_strips) \
{ \
    pddl_files_t files; \
    bor_err_t err = BOR_ERR_INIT; \
    if (pddlFiles(&files, "pddl-data/", P, &err) != 0){ \
        borErrPrint(&err, 1, stderr); \
        return; \
    } \
    run(files.domain_pddl, files.problem_pddl, #N, 0, 1); \
}
#include "h1_prob.h"
