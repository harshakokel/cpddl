#ifndef TEST_ADMISSIBLE_H
#define TEST_ADMISSIBLE_H

TEST(testAdmissibleSetUp);

#define P(N, P) \
    TEST(testAdmissibleLMCut_##N); \
    TEST(testAdmissibleFlow_##N);
#include "admissible_prob.h"

TEST_SUITE(TSAdmissible) {
    TEST_ADD(testAdmissibleSetUp),
#define P(N, P) \
    TEST_ADD(testAdmissibleLMCut_##N), \
    TEST_ADD(testAdmissibleFlow_##N),
#include "admissible_prob.h"
    TEST_SUITE_CLOSURE
};
#endif

