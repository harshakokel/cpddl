#include <sys/time.h>
#include <sys/resource.h>
#include <cu/cu.h>
#include "pddl/pddl.h"

static int isInMGroup(int fact, const pddl_mgroups_t *mgroups)
{
    for (int i = 0; i < mgroups->mgroup_size; ++i){
        if (borISetIn(fact, &mgroups->mgroup[i].mgroup))
            return 1;
    }
    return 0;
}

static void run(const char *domain_fn, const char *problem_fn,
                const char *outfn,
                int compile_away_cond_eff,
                int compile_away_cond_eff_strips)
{
    pddl_config_t cfg = PDDL_CONFIG_INIT;
    pddl_t pddl;
    bor_err_t err = BOR_ERR_INIT;
    int ret;

    cfg.force_adl = 1;
    ret = pddlInit(&pddl, domain_fn, problem_fn, &cfg, &err);
    assertEquals(ret, 0);
    if (ret != 0){
        borErrPrint(&err, 1, stderr);
        return;
    }
    pddlCheckSizeTypes(&pddl);

    pddlNormalize(&pddl);
    if (compile_away_cond_eff)
        pddlCompileAwayNonStaticCondEff(&pddl);

    pddl_lifted_mgroups_infer_limits_t infer_limit
                = PDDL_LIFTED_MGROUPS_INFER_LIMITS_INIT;
    pddl_lifted_mgroups_t lmgs;
    pddlLiftedMGroupsInit(&lmgs);
    pddlLiftedMGroupsInferFAMGroups(&pddl, &infer_limit, &lmgs, &err);
    pddlLiftedMGroupsSetExactlyOne(&pddl, &lmgs, &err);
    pddlLiftedMGroupsSetStatic(&pddl, &lmgs, &err);
    //pddlLiftedMGroupsPrint(&pddl, &lmgs, stdout);
    fflush(stdout);

    pddl_ground_config_t ground_cfg = PDDL_GROUND_CONFIG_INIT;
    pddl_strips_t strips;
    ret = pddlStripsGround(&strips, &pddl, &ground_cfg, &err);
    assertEquals(ret, 0);
    if (compile_away_cond_eff_strips)
        pddlStripsCompileAwayCondEff(&strips);

    pddl_mgroups_t mgroups;
    pddlMGroupsInitEmpty(&mgroups);
    pddlMGroupsGround(&mgroups, &pddl, &lmgs, &strips);

    pddl_mutex_pairs_t mutex;
    pddlMutexPairsInitStrips(&mutex, &strips);
    pddlMutexPairsAddMGroups(&mutex, &mgroups);

    BOR_ISET(unreachable_ops);
    pddlStripsFindUnreachableOps(&strips, &mutex, &unreachable_ops, &err);
    pddlStripsReduce(&strips, NULL, &unreachable_ops);
    pddlStripsRemoveUselessDelEffs(&strips, &mutex, NULL, &err);
    borISetFree(&unreachable_ops);

    BOR_ISET(invertible);
    pddlRSEInvertibleFacts(&strips, &mgroups, &invertible, &err);

    if (borISetSize(&invertible) > 0)
        printf("RSE-Invertible facts:\n");
    int fact;
    BOR_ISET_FOR_EACH(&invertible, fact){
        printf("%d:(%s)", fact, strips.fact.fact[fact]->name);
        if (isInMGroup(fact, &mgroups))
            printf(" in-fam-group");
        printf("\n");
    }
    fflush(stdout);
    borISetFree(&invertible);

    pddlMutexPairsFree(&mutex);
    pddlMGroupsFree(&mgroups);
    pddlLiftedMGroupsFree(&lmgs);
    pddlStripsFree(&strips);
    pddlFree(&pddl);
}

static void setMemLimit(void)
{
    struct rlimit mem_limit;

    mem_limit.rlim_cur = mem_limit.rlim_max = 512 * 1024 * 1024;
    mem_limit.rlim_cur = mem_limit.rlim_max = 2024 * 1024 * 1024;
    setrlimit(RLIMIT_AS, &mem_limit);
}

TEST(testInvertibilitySetUp)
{
    setMemLimit();
}

#define P(N, P) \
TEST(testInvertibility_##N) \
{ \
    pddl_files_t files; \
    bor_err_t err = BOR_ERR_INIT; \
    if (pddlFiles(&files, "pddl-data/", P, &err) != 0){ \
        borErrPrint(&err, 1, stderr); \
        return; \
    } \
    run(files.domain_pddl, files.problem_pddl, #N, 0, 1); \
}

#include "invertibility_prob.h"

