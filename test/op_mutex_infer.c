#include <sys/time.h>
#include <sys/resource.h>
#include <cu/cu.h>
#include <boruvka/rand.h>
#include "pddl/pddl.h"

static void setMemLimit(void)
{
    struct rlimit mem_limit;

    mem_limit.rlim_cur = mem_limit.rlim_max = 2024 * 1024 * 1024;
    setrlimit(RLIMIT_AS, &mem_limit);
}

TEST(testOpMutexInferSetUp)
{
    setMemLimit();
}

static void checkPlan(const bor_iset_t *plan,
                      const pddl_op_mutex_pairs_t *opm)
{
    int o1, o2;
    BOR_ISET_FOR_EACH(plan, o1){
        BOR_ISET_FOR_EACH(plan, o2){
            assertFalse(pddlOpMutexPairsIsMutex(opm, o1, o2));
        }
    }
}

static void checkDominance(const pddl_op_mutex_pairs_t *opm1,
                           const pddl_op_mutex_pairs_t *opm2)
{
    int o1, o2;
    PDDL_OP_MUTEX_PAIRS_FOR_EACH(opm1, o1, o2){
        assertTrue(pddlOpMutexPairsIsMutex(opm2, o1, o2));
    }
}

static int testInfer(const char *fn)
{
    bor_err_t err = BOR_ERR_INIT;
    borErrWarnEnable(&err, NULL);
    borErrInfoEnable(&err, NULL);

    pddl_files_t files;
    if (pddlFiles1(&files, fn, &err) != 0){
        borErrPrint(&err, 1, stderr);
        return -1;
    }

    // Parse PDDL
    pddl_config_t pddl_cfg = PDDL_CONFIG_INIT;
    pddl_cfg.force_adl = 1;
    pddl_t pddl;
    if (pddlInit(&pddl, files.domain_pddl, files.problem_pddl,
                 &pddl_cfg, &err) != 0){
        fprintf(stderr, "Error: ");
        borErrPrint(&err, 1, stderr);
        return -1;
    }
    pddlNormalize(&pddl);
    pddlCheckSizeTypes(&pddl);

    // Lifted mgroups
    pddl_lifted_mgroups_infer_limits_t lifted_mgroups_limits
                = PDDL_LIFTED_MGROUPS_INFER_LIMITS_INIT;
    pddl_lifted_mgroups_t lifted_mgroups;
    pddlLiftedMGroupsInit(&lifted_mgroups);
    pddlLiftedMGroupsInferFAMGroups(&pddl, &lifted_mgroups_limits,
                                    &lifted_mgroups, &err);
    pddlLiftedMGroupsSetExactlyOne(&pddl, &lifted_mgroups, &err);
    pddlLiftedMGroupsSetStatic(&pddl, &lifted_mgroups, &err);

    // Ground to STRIPS
    pddl_ground_config_t ground_cfg = PDDL_GROUND_CONFIG_INIT;
    ground_cfg.lifted_mgroups = &lifted_mgroups;
    ground_cfg.prune_op_pre_mutex = 1;
    ground_cfg.prune_op_dead_end = 1;
    pddl_strips_t strips;
    if (pddlStripsGround(&strips, &pddl, &ground_cfg, &err) != 0){
        BOR_INFO2(&err, "Grounding failed.");
        fprintf(stderr, "Error: ");
        borErrPrint(&err, 1, stderr);
        return -1;
    }
    if (strips.has_cond_eff){
        BOR_INFO2(&err, "Has conditional effects -- terminating...");
        return -1;
    }

    // Prune strips
    if (!strips.has_cond_eff){
        BOR_ISET(rm_fact);
        BOR_ISET(rm_op);

        pddl_mutex_pairs_t mutex;
        pddlMutexPairsInitStrips(&mutex, &strips);
        pddlH2(&strips, &mutex, &rm_fact, &rm_op, 0., &err);
        if (borISetSize(&rm_fact) > 0 || borISetSize(&rm_op) > 0)
            pddlStripsReduce(&strips, &rm_fact, &rm_op);
        pddlMutexPairsFree(&mutex);

        borISetEmpty(&rm_fact);
        borISetEmpty(&rm_op);

        if (pddlIrrelevanceAnalysis(&strips, &rm_fact, &rm_op, NULL, &err) != 0){
            BOR_INFO2(&err, "Irrelevance analysis failed.");
            fprintf(stderr, "Error: ");
            borErrPrint(&err, 1, stderr);
            return -1;
        }
        if (borISetSize(&rm_fact) > 0 || borISetSize(&rm_op) > 0)
            pddlStripsReduce(&strips, &rm_fact, &rm_op);
        borISetFree(&rm_fact);
        borISetFree(&rm_op);
    }

    BOR_ISET(plan_ops);
    pddl_plan_file_strips_t plan;
    char plan_fn[128];
    sprintf(plan_fn, "%s.plan", fn);
    if (pddlPlanFileStripsInit(&plan, &strips, plan_fn, &err) == 0){
        int op;
        BOR_IARR_FOR_EACH(&plan.op, op)
            borISetAdd(&plan_ops, op);
    }
    pddlPlanFileStripsFree(&plan);

    // Ground mutex groups
    pddl_mgroups_t mgroups;
    pddlMGroupsGround(&mgroups, &pddl, &lifted_mgroups, &strips);
    pddlMGroupsSetExactlyOne(&mgroups, &strips);
    pddlMGroupsSetGoal(&mgroups, &strips);

    pddl_mg_strips_t mg_strips;
    pddlMGStripsInit(&mg_strips, &strips, &mgroups);

    pddl_mutex_pairs_t mutex;
    pddlMutexPairsInitStrips(&mutex, &mg_strips.strips);
    pddlH2(&mg_strips.strips, &mutex, NULL, NULL, 0., &err);
    pddlMutexPairsAddMGroups(&mutex, &mg_strips.mg);

    int ret;

    pddl_op_mutex_pairs_t opm_ts, opm_ts_fam, opm_ts2, opm_h2, opm_h2_op;
    pddl_op_mutex_pairs_t opm_h3;
    pddlOpMutexPairsInit(&opm_ts, &mg_strips.strips);
    pddlOpMutexPairsInit(&opm_ts_fam, &mg_strips.strips);
    pddlOpMutexPairsInit(&opm_ts2, &mg_strips.strips);
    pddlOpMutexPairsInit(&opm_h2, &mg_strips.strips);
    pddlOpMutexPairsInit(&opm_h2_op, &mg_strips.strips);
    pddlOpMutexPairsInit(&opm_h3, &mg_strips.strips);

    ret = pddlOpMutexInferTransSystems(&opm_ts, &mg_strips, &mutex,
                                       1, 0, 0, &err);
    assertTrue(ret == 0)
    checkPlan(&plan_ops, &opm_ts);
    printf("TS: %d\n", opm_ts.num_op_mutex_pairs);

    ret = pddlOpMutexInferFAMGroups(&opm_ts_fam, &mg_strips.strips,
                                    &mg_strips.mg, &err);
    assertTrue(ret == 0);
    checkPlan(&plan_ops, &opm_ts_fam);
    printf("FAM: %d\n", opm_ts_fam.num_op_mutex_pairs);

    ret = pddlOpMutexInferTransSystems(&opm_ts2, &mg_strips, &mutex,
                                       2, 0, 0, &err);
    assertTrue(ret == 0);
    checkPlan(&plan_ops, &opm_ts2);
    printf("TS2: %d\n", opm_ts2.num_op_mutex_pairs);

    ret = pddlOpMutexInferHmOpFactCompilation(&opm_h2, 2, &mg_strips.strips,
                                              &err);
    assertTrue(ret == 0);
    checkPlan(&plan_ops, &opm_h2);
    printf("op-fact-h^2: %d\n", opm_h2.num_op_mutex_pairs);

    ret = pddlOpMutexInferHmFromEachOp(&opm_h2_op, 2, &mg_strips.strips,
                                       &mutex, NULL, &err);
    checkPlan(&plan_ops, &opm_h2_op);
    printf("2-h^2-op: %d\n", opm_h2_op.num_op_mutex_pairs);

    /*
    ret = pddlOpMutexInferHmOpFactCompilation(&opm_h3, 3, &mg_strips.strips,
                                              &err);
    assertTrue(ret == 0);
    checkPlan(&plan_ops, &opm_h3);
    printf("op-fact-h^3: %d\n", opm_h3.num_op_mutex_pairs);
    */

    checkDominance(&opm_ts_fam, &opm_ts);
    checkDominance(&opm_ts, &opm_ts2);
    checkDominance(&opm_ts, &opm_h2);
    checkDominance(&opm_h2, &opm_h2_op);
    //checkDominance(&opm_h2, &opm_h3);

    pddlOpMutexPairsFree(&opm_ts);
    pddlOpMutexPairsFree(&opm_ts_fam);
    pddlOpMutexPairsFree(&opm_ts2);
    pddlOpMutexPairsFree(&opm_h2);
    pddlOpMutexPairsFree(&opm_h2_op);
    pddlOpMutexPairsFree(&opm_h3);
    borISetFree(&plan_ops);

    pddlMGStripsFree(&mg_strips);
    pddlMutexPairsFree(&mutex);
    pddlMGroupsFree(&mgroups);
    pddlStripsFree(&strips);
    pddlLiftedMGroupsFree(&lifted_mgroups);
    pddlFree(&pddl);
    return 0;
}


#define P(N, P) \
TEST(testOpMutexInfer_##N) \
{ \
    testInfer("pddl-data/ipc-opt-noce/" P); \
}

#include "op_mutex_infer_prob.h"

