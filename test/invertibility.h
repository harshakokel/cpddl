#ifndef TEST_Invertibility_H
#define TEST_Invertibility_H

TEST(testInvertibilitySetUp);

#define P(N, P) \
    TEST(testInvertibility_##N);
#include "invertibility_prob.h"

TEST_SUITE(TSInvertibility) {
    TEST_ADD(testInvertibilitySetUp),
#define P(N, P) \
    TEST_ADD(testInvertibility_##N),
#include "invertibility_prob.h"
    TEST_SUITE_CLOSURE
};
#endif

