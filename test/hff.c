#include <sys/time.h>
#include <sys/resource.h>
#include <cu/cu.h>
#include "pddl/pddl.h"

static void run(const char *domain_fn, const char *problem_fn,
                const char *outfn,
                int compile_away_cond_eff,
                int compile_away_cond_eff_strips)
{
    pddl_config_t cfg = PDDL_CONFIG_INIT;
    pddl_ground_config_t ground_cfg = PDDL_GROUND_CONFIG_INIT;
    pddl_t pddl;
    bor_err_t err = BOR_ERR_INIT;
    pddl_strips_t strips;
    int ret;

    cfg.force_adl = 1;
    ret = pddlInit(&pddl, domain_fn, problem_fn, &cfg, &err);
    assertEquals(ret, 0);
    if (ret != 0){
        borErrPrint(&err, 1, stderr);
        return;
    }
    pddlCheckSizeTypes(&pddl);

    pddlNormalize(&pddl);
    if (compile_away_cond_eff)
        pddlCompileAwayNonStaticCondEff(&pddl);

    pddl_lifted_mgroups_infer_limits_t lifted_mgroups_limits
            = PDDL_LIFTED_MGROUPS_INFER_LIMITS_INIT;
    pddl_lifted_mgroups_t lifted_mgroups;
    pddlLiftedMGroupsInit(&lifted_mgroups);
    pddlLiftedMGroupsInferFAMGroups(&pddl, &lifted_mgroups_limits,
                                    &lifted_mgroups, &err);
    pddlLiftedMGroupsSetExactlyOne(&pddl, &lifted_mgroups, &err);
    pddlLiftedMGroupsSetStatic(&pddl, &lifted_mgroups, &err);

    ground_cfg.lifted_mgroups = &lifted_mgroups;
    ground_cfg.prune_op_pre_mutex = 1;
    ground_cfg.prune_op_dead_end = 1;
    ret = pddlStripsGround(&strips, &pddl, &ground_cfg, &err);
    assertEquals(ret, 0);
    if (compile_away_cond_eff_strips)
        pddlStripsCompileAwayCondEff(&strips);

    BOR_ISET(irr_facts);
    BOR_ISET(irr_ops);
    pddlIrrelevanceAnalysis(&strips, &irr_facts, &irr_ops, NULL, &err);
    pddlStripsReduce(&strips, &irr_facts, &irr_ops);
    borISetFree(&irr_facts);
    borISetFree(&irr_ops);

    pddl_mgroups_t mgroups;
    pddl_mutex_pairs_t mutex;

    pddlMGroupsGround(&mgroups, &pddl, &lifted_mgroups, &strips);
    pddlMGroupsSetExactlyOne(&mgroups, &strips);
    pddlMGroupsSetGoal(&mgroups, &strips);

    pddlMutexPairsInitStrips(&mutex, &strips);
    pddlMutexPairsAddMGroups(&mutex, &mgroups);

    pddl_fdr_t fdr;
    unsigned fdr_var_flag = PDDL_FDR_VARS_LARGEST_FIRST;
    unsigned fdr_flag = 0u;
    pddlFDRInitFromStrips(&fdr, &strips, &mgroups, &mutex, fdr_var_flag,
                          fdr_flag, &err);

    borErrInfoEnable(&err, stdout);
    BOR_IARR(plan);
    pddl_hff_t hff;
    pddlHFFInit(&hff, &fdr);
    int heur = pddlHFFPlan(&hff, fdr.init, &fdr.var, &plan);
    fprintf(stdout, "heur-ff: %d\n", heur);
    if (heur != PDDL_COST_DEAD_END){
        assertTrue(pddlFDRIsRelaxedPlan(&fdr, fdr.init, &plan, &err));
    }
    pddlHFFFree(&hff);
    borIArrFree(&plan);

    pddl_hmax_t hmax;
    pddlHMaxInit(&hmax, &fdr);
    int heur_max = pddlHMax(&hmax, fdr.init, &fdr.var);
    fprintf(stdout, "heur-max: %d\n", heur_max);
    pddlHMaxFree(&hmax);

    pddl_hadd_t hadd;
    pddlHAddInit(&hadd, &fdr);
    int heur_add = pddlHAdd(&hadd, fdr.init, &fdr.var);
    fprintf(stdout, "heur-add: %d\n", heur_add);
    pddlHAddFree(&hadd);

    assertTrue(heur_max <= heur);
    assertTrue(heur <= heur_add);

    pddlFDRFree(&fdr);
    pddlMutexPairsFree(&mutex);
    pddlMGroupsFree(&mgroups);
    pddlLiftedMGroupsFree(&lifted_mgroups);
    pddlStripsFree(&strips);
    pddlFree(&pddl);
}

static void setMemLimit(void)
{
    struct rlimit mem_limit;

    mem_limit.rlim_cur = mem_limit.rlim_max = 512 * 1024 * 1024;
    mem_limit.rlim_cur = mem_limit.rlim_max = 2024 * 1024 * 1024;
    setrlimit(RLIMIT_AS, &mem_limit);
}

TEST(testHFFSetUp)
{
    setMemLimit();
}

#define P(N, P) \
TEST(testHFF_##N) \
{ \
    pddl_files_t files; \
    bor_err_t err = BOR_ERR_INIT; \
    if (pddlFiles(&files, "pddl-data/", P, &err) != 0){ \
        borErrPrint(&err, 1, stderr); \
        return; \
    } \
    /* TODO */ \
    run(files.domain_pddl, files.problem_pddl, #N, 0, 1); \
}

#define NCE(N, P) \
TEST(testHFF_##N##_noce_strips) \
{ \
    pddl_files_t files; \
    bor_err_t err = BOR_ERR_INIT; \
    if (pddlFiles(&files, "pddl-data/", P, &err) != 0){ \
        borErrPrint(&err, 1, stderr); \
        return; \
    } \
    run(files.domain_pddl, files.problem_pddl, #N, 0, 1); \
}
#include "hff_prob.h"
