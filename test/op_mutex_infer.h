#ifndef TEST_OP_MUTEX_INFER_H
#define TEST_OP_MUTEX_INFER_H

TEST(testOpMutexInferSetUp);

#define P(N, P) \
    TEST(testOpMutexInfer_##N);
#include "op_mutex_infer_prob.h"

TEST_SUITE(TSOpMutexInfer) {
    TEST_ADD(testOpMutexInferSetUp),
#define P(N, P) \
    TEST_ADD(testOpMutexInfer_##N),
#include "op_mutex_infer_prob.h"
    TEST_SUITE_CLOSURE
};
#endif


