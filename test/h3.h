#ifndef TEST_H3_H
#define TEST_H3_H

TEST(testH3SetUp);

#define P(N, P) \
    TEST(testH3_##N);
#define NCE(N, P) \
    TEST(testH3_##N##_noce_strips);
#include "h3_prob.h"

TEST_SUITE(TSH3) {
    TEST_ADD(testH3SetUp),
#define P(N, P) \
    TEST_ADD(testH3_##N),
#define NCE(N, P) \
    TEST_ADD(testH3_##N##_noce_strips),
#include "h3_prob.h"
    TEST_SUITE_CLOSURE
};
#endif
