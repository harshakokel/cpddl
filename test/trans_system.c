#include <sys/time.h>
#include <sys/resource.h>
#include <cu/cu.h>
#include <boruvka/rand.h>
#include "pddl/pddl.h"

static void setMemLimit(void)
{
    struct rlimit mem_limit;

    mem_limit.rlim_cur = mem_limit.rlim_max = 512 * 1024 * 1024;
    mem_limit.rlim_cur = mem_limit.rlim_max = 2024 * 1024 * 1024;
    setrlimit(RLIMIT_AS, &mem_limit);
}

TEST(testTransSystemSetUp)
{
    setMemLimit();
}

static int selectRandomApplicableOp(bor_rand_t *rnd,
                                    const pddl_strips_t *strips,
                                    const bor_iset_t *state)
{
    BOR_ISET(app);
    for (int op_id = 0; op_id < strips->op.op_size; ++op_id){
        const pddl_strips_op_t *op = strips->op.op[op_id];
        if (borISetIsSubset(&op->pre, state))
            borISetAdd(&app, op_id);
    }

    int op = -1;
    if (borISetSize(&app) > 0){
        int idx = borRand(rnd, 0, borISetSize(&app));
        op = borISetGet(&app, idx);
    }
    borISetFree(&app);
    return op;
}

static void checkTransition(const pddl_trans_systems_t *tss,
                            int ts_id,
                            int from,
                            int to,
                            int label)
{
    const pddl_trans_system_t *ts = tss->ts[ts_id];
    for (int ltri = 0; ltri < ts->trans.trans_size; ++ltri){
        const pddl_label_set_t *lbl = ts->trans.trans[ltri].label;
        const pddl_transitions_t *trs = &ts->trans.trans[ltri].trans;
        for (int tri = 0; tri < trs->trans_size; ++tri){
            if (from == trs->trans[tri].from && to == trs->trans[tri].to){
                assertTrue(borISetIn(label, &lbl->label));
                return;
            }
        }
    }
    //fprintf(stderr, "E %d %d->%d %d\n", ts_id, from, to, label);
    assertTrueM(0, "The transition not found");
}

static void testRandomWalk(bor_rand_t *rnd,
                           const pddl_strips_t *strips,
                           pddl_lm_cut_t *lmc,
                           const pddl_trans_systems_t *tss,
                           int ts_id,
                           int length)
{
    //fprintf(stderr, "Random Walk for %d\n", ts_id);
    BOR_ISET(state);
    borISetUnion(&state, &strips->init);

    int state_id = pddlTransSystemsStripsState(tss, ts_id, &state);
    assertEquals(state_id, tss->ts[ts_id]->init_state);

    //fprintf(stderr, "init-id: %d\n", state_id);
    int op_id = selectRandomApplicableOp(rnd, strips, &state);
    assertTrue(!borISetIn(op_id, &tss->dead_labels));
    for (int i = 0; i < length && op_id >= 0; ++i){
        const pddl_strips_op_t *op = strips->op.op[op_id];
        borISetMinus(&state, &op->del_eff);
        borISetUnion(&state, &op->add_eff);
        int next_id = pddlTransSystemsStripsState(tss, ts_id, &state);
        //fprintf(stderr, "step %d: %d->%d [%d:(%s)]\n", i, state_id, next_id,
        //                op_id, op->name);
        if (next_id < 0){
            int heur = pddlLMCutStrips(lmc, &state, NULL, NULL);
            if (heur != PDDL_COST_DEAD_END){
                int is_dead = pddlH2IsDeadEnd(strips, &state);
                if (!is_dead){
                    fprintf(stderr, "Unverified dead-end:");
                    int fact;
                    BOR_ISET_FOR_EACH(&state, fact)
                        fprintf(stderr, " %d", fact);
                    fprintf(stderr, "\n");
                }
            }

            borISetFree(&state);
            return;
        }
        checkTransition(tss, ts_id, state_id, next_id, op_id);

        op_id = selectRandomApplicableOp(rnd, strips, &state);
        assertTrue(!borISetIn(op_id, &tss->dead_labels));
        state_id = next_id;
    }
    borISetFree(&state);
}

static void testAbstrMap(pddl_trans_systems_t *tss, int ts_id)
{
    const pddl_trans_system_t *ts = tss->ts[ts_id];
    pddl_trans_system_abstr_map_t map;
    pddlTransSystemAbstrMapInit(&map, ts->num_states);
    assertTrue(ts->num_states == map.num_states);
    assertTrue(map.map_num_states < 0);

    int *dist = BOR_ALLOC_ARR(int, ts->num_states);
    pddl_trans_system_graph_t graph;

    pddlTransSystemGraphInit(&graph, ts);
    pddlTransSystemGraphFwDist(&graph, dist);
    for (int s = 0; s < ts->num_states; ++s){
        if (dist[s] < 0)
            pddlTransSystemAbstrMapPruneState(&map, s);
    }
    pddlTransSystemGraphBwDist(&graph, dist);
    for (int s = 0; s < ts->num_states; ++s){
        if (dist[s] < 0)
            pddlTransSystemAbstrMapPruneState(&map, s);
    }

    pddl_set_iset_t comp;
    pddlSetISetInit(&comp);
    pddlTransSystemGraphFwSCC(&graph, &comp);


    for (int ci = 0; ci < pddlSetISetSize(&comp); ++ci){
        const bor_iset_t *c = pddlSetISetGet(&comp, ci);
        if (borISetSize(c) > 1)
            pddlTransSystemAbstrMapCondense(&map, c);
    }
    pddlSetISetFree(&comp);

    /*
    fprintf(stdout, "Before[%d]:", map.map_num_states);
    for (int s = 0; s < map.num_states; ++s){
        fprintf(stdout, " %d", map.map[s]);
    }
    fprintf(stdout, "\n");
    */

    pddlTransSystemAbstrMapFinalize(&map);
    fprintf(stdout, "    Map[%d]:", map.map_num_states);
    for (int s = 0; s < map.num_states; ++s){
        fprintf(stdout, " %d", map.map[s]);
    }
    fprintf(stdout, "\n");
    fflush(stdout);

    if (!map.is_identity){
        fprintf(stdout, "    Abstracted ");
        int abstr_tsi = pddlTransSystemsCloneTransSystem(tss, ts_id);
        pddlTransSystemsAbstract(tss, abstr_tsi, &map);
        pddlTransSystemPrintDebug2(tss, abstr_tsi, stdout);

        if (tss->ts[abstr_tsi]->num_states > 1){
            const pddl_trans_system_t *ats = tss->ts[abstr_tsi];
            pddl_trans_system_graph_t graph;
            pddlTransSystemGraphInit(&graph, ats);

            bor_iset_t *reach = BOR_CALLOC_ARR(bor_iset_t, graph.num_states);
            pddlTransSystemGraphFwReachability(&graph, reach, 0);
            fprintf(stdout, "    Reachable:");
            for (int i = 0; i < graph.num_states; ++i){
                fprintf(stdout, " [%d]<-[", i);
                pddlISetPrintCompressed(reach + i, stdout);
                fprintf(stdout, "]");
            }
            fprintf(stdout, "\n");
            for (int i = 0; i < graph.num_states; ++i)
                borISetFree(reach + i);
            BOR_FREE(reach);
            pddlTransSystemGraphFree(&graph);
        }
    }

    pddlTransSystemAbstrMapFree(&map);
    pddlTransSystemGraphFree(&graph);
    BOR_FREE(dist);
}

static void printTS(const pddl_trans_systems_t *tss,
                    int tsi,
                    const pddl_mg_strips_t *mg_strips)
{
    const pddl_trans_system_t *ts = tss->ts[tsi];
    if (tsi != 0)
        fprintf(stdout, "\n");

    pddlTransSystemPrintDebug2(tss, tsi, stdout);

    int *dist = BOR_ALLOC_ARR(int, ts->num_states);
    pddl_trans_system_graph_t graph;
    pddlTransSystemGraphInit(&graph, ts);
    pddlTransSystemGraphFwDist(&graph, dist);
    fprintf(stdout, "    fw-dist:");
    for (int i = 0; i < ts->num_states; ++i)
        fprintf(stdout, " %d", dist[i]);
    fprintf(stdout, "\n");

    pddlTransSystemGraphBwDist(&graph, dist);
    fprintf(stdout, "    bw-dist:");
    for (int i = 0; i < ts->num_states; ++i)
        fprintf(stdout, " %d", dist[i]);
    fprintf(stdout, "\n");

    pddl_set_iset_t comp;
    pddlSetISetInit(&comp);
    pddlTransSystemGraphFwSCC(&graph, &comp);
    fprintf(stdout, "    fw-scc:");
    for (int ci = 0; ci < pddlSetISetSize(&comp); ++ci){
        const bor_iset_t *c = pddlSetISetGet(&comp, ci);
        fprintf(stdout, " [");
        pddlISetPrintCompressed(c, stdout);
        fprintf(stdout, "]");
    }
    fprintf(stdout, "\n");
    pddlSetISetFree(&comp);

    pddlSetISetInit(&comp);
    pddlTransSystemGraphBwSCC(&graph, &comp);
    fprintf(stdout, "    bw-scc:");
    for (int ci = 0; ci < pddlSetISetSize(&comp); ++ci){
        const bor_iset_t *c = pddlSetISetGet(&comp, ci);
        fprintf(stdout, " [");
        pddlISetPrintCompressed(c, stdout);
        fprintf(stdout, "]");
    }
    fprintf(stdout, "\n");
    pddlSetISetFree(&comp);

    pddlTransSystemGraphFree(&graph);
    BOR_FREE(dist);
}

static void testInit(const pddl_mg_strips_t *mg_strips,
                     const pddl_mutex_pairs_t *mutex)
{
    //pddlStripsPrintDebug(&mg_strips->strips, stdout);

    pddl_trans_systems_t tss;
    pddlTransSystemsInit(&tss, mg_strips, mutex);
    assertTrue(tss.ts_size > 0);

    pddlTransSystemsCollectDeadLabelsFromAll(&tss);
    pddlTransSystemsRemoveDeadLabelsFromAll(&tss);

    int ts_size = tss.ts_size;
    for (int tsi = 0; tsi < ts_size; ++tsi){
        const pddl_trans_system_t *ts = tss.ts[tsi];
        assertTrue(ts->num_states > 1);
    }

    for (int tsi = 0; tsi < ts_size; ++tsi){
        const pddl_trans_system_t *ts = tss.ts[tsi];
        assertTrue(ts->num_states > 1);
        printTS(&tss, tsi, mg_strips);
        testAbstrMap(&tss, tsi);
    }

    pddl_lm_cut_t lmc;
    pddlLMCutInitStrips(&lmc, &mg_strips->strips, 0, 0);

    bor_rand_t rnd;
    //borRandInitSeed(&rnd, 123);
    borRandInit(&rnd);
    ts_size = BOR_MIN(tss.ts_size, 30);
    for (int tsi = 0; tsi < ts_size; ++tsi){
        for (int i = 0; i < 30; ++i)
            testRandomWalk(&rnd, &mg_strips->strips, &lmc, &tss, tsi, 100);
    }

    pddlLMCutFree(&lmc);

    pddlTransSystemsFree(&tss);
}

static void testMerge(const pddl_mg_strips_t *mg_strips,
                      const pddl_mutex_pairs_t *mutex)
{
    //pddlStripsPrintDebug(&mg_strips->strips, stdout);

    pddl_trans_systems_t tss;
    pddlTransSystemsInit(&tss, mg_strips, mutex);
    assertTrue(tss.ts_size > 0);

    pddlTransSystemsCollectDeadLabelsFromAll(&tss);
    pddlTransSystemsRemoveDeadLabelsFromAll(&tss);

    int merge_tsi_first = tss.ts_size;
    int max_ts_size = BOR_MIN(tss.ts_size, 3);
    for (int i1 = 0; i1 < max_ts_size; ++i1){
        for (int i2 = i1 + 1; i2 < max_ts_size; ++i2){
            pddlTransSystemsMerge(&tss, i1, i2);
        }
    }

    pddlTransSystemsCollectDeadLabelsFromAll(&tss);
    pddlTransSystemsRemoveDeadLabelsFromAll(&tss);

    int ts_size = tss.ts_size;
    for (int tsi = merge_tsi_first; tsi < ts_size; ++tsi){
        const pddl_trans_system_t *ts = tss.ts[tsi];
        assertTrue(ts->num_states > 1);
        printTS(&tss, tsi, mg_strips);
        testAbstrMap(&tss, tsi);
    }

    pddl_lm_cut_t lmc;
    pddlLMCutInitStrips(&lmc, &mg_strips->strips, 0, 0);

    bor_rand_t rnd;
    //borRandInitSeed(&rnd, 123);
    borRandInit(&rnd);
    for (int tsi = merge_tsi_first; tsi < tss.ts_size; ++tsi){
        for (int i = 0; i < 30; ++i)
            testRandomWalk(&rnd, &mg_strips->strips, &lmc, &tss, tsi, 100);
    }

    pddlLMCutFree(&lmc);

    pddlTransSystemsFree(&tss);
}

static int test(const char *fn,
                void (*test_cb)(const pddl_mg_strips_t *,
                                const pddl_mutex_pairs_t *))
{
    bor_err_t err = BOR_ERR_INIT;
    borErrWarnEnable(&err, NULL);
    borErrInfoEnable(&err, NULL);

    pddl_files_t files;
    if (pddlFiles1(&files, fn, &err) != 0){
        borErrPrint(&err, 1, stderr);
        return -1;
    }

    // Parse PDDL
    pddl_config_t pddl_cfg = PDDL_CONFIG_INIT;
    pddl_cfg.force_adl = 1;
    pddl_t pddl;
    if (pddlInit(&pddl, files.domain_pddl, files.problem_pddl,
                 &pddl_cfg, &err) != 0){
        fprintf(stderr, "Error: ");
        borErrPrint(&err, 1, stderr);
        return -1;
    }
    pddlNormalize(&pddl);
    pddlCheckSizeTypes(&pddl);

    // Lifted mgroups
    pddl_lifted_mgroups_infer_limits_t lifted_mgroups_limits
                = PDDL_LIFTED_MGROUPS_INFER_LIMITS_INIT;
    pddl_lifted_mgroups_t lifted_mgroups;
    pddlLiftedMGroupsInit(&lifted_mgroups);
    pddlLiftedMGroupsInferFAMGroups(&pddl, &lifted_mgroups_limits,
                                    &lifted_mgroups, &err);
    pddlLiftedMGroupsSetExactlyOne(&pddl, &lifted_mgroups, &err);
    pddlLiftedMGroupsSetStatic(&pddl, &lifted_mgroups, &err);

    // Ground to STRIPS
    pddl_ground_config_t ground_cfg = PDDL_GROUND_CONFIG_INIT;
    ground_cfg.lifted_mgroups = &lifted_mgroups;
    ground_cfg.prune_op_pre_mutex = 1;
    ground_cfg.prune_op_dead_end = 1;
    pddl_strips_t strips;
    if (pddlStripsGround(&strips, &pddl, &ground_cfg, &err) != 0){
        BOR_INFO2(&err, "Grounding failed.");
        fprintf(stderr, "Error: ");
        borErrPrint(&err, 1, stderr);
        return -1;
    }
    if (strips.has_cond_eff){
        BOR_INFO2(&err, "Has conditional effects -- terminating...");
        return -1;
    }

    // Prune strips
    if (!strips.has_cond_eff){
        BOR_ISET(rm_fact);
        BOR_ISET(rm_op);

        pddl_mutex_pairs_t mutex;
        pddlMutexPairsInitStrips(&mutex, &strips);
        pddlH2(&strips, &mutex, &rm_fact, &rm_op, 0., &err);
        if (borISetSize(&rm_fact) > 0 || borISetSize(&rm_op) > 0)
            pddlStripsReduce(&strips, &rm_fact, &rm_op);
        pddlMutexPairsFree(&mutex);

        borISetEmpty(&rm_fact);
        borISetEmpty(&rm_op);

        if (pddlIrrelevanceAnalysis(&strips, &rm_fact, &rm_op, NULL, &err) != 0){
            BOR_INFO2(&err, "Irrelevance analysis failed.");
            fprintf(stderr, "Error: ");
            borErrPrint(&err, 1, stderr);
            return -1;
        }
        if (borISetSize(&rm_fact) > 0 || borISetSize(&rm_op) > 0)
            pddlStripsReduce(&strips, &rm_fact, &rm_op);
        borISetFree(&rm_fact);
        borISetFree(&rm_op);
    }

    // Ground mutex groups
    pddl_mgroups_t mgroups;
    pddlMGroupsGround(&mgroups, &pddl, &lifted_mgroups, &strips);
    pddlMGroupsSetExactlyOne(&mgroups, &strips);
    pddlMGroupsSetGoal(&mgroups, &strips);

    pddl_mg_strips_t mg_strips;
    pddlMGStripsInit(&mg_strips, &strips, &mgroups);

    pddl_mutex_pairs_t mutex;
    pddlMutexPairsInitStrips(&mutex, &mg_strips.strips);
    pddlH2(&mg_strips.strips, &mutex, NULL, NULL, 0., &err);
    pddlMutexPairsAddMGroups(&mutex, &mg_strips.mg);

    test_cb(&mg_strips, &mutex);

    pddlMGStripsFree(&mg_strips);
    pddlMutexPairsFree(&mutex);
    pddlMGroupsFree(&mgroups);
    pddlStripsFree(&strips);
    pddlLiftedMGroupsFree(&lifted_mgroups);
    pddlFree(&pddl);
    return 0;
}


#define P(N, P) \
TEST(testTransSystemInit_##N) \
{ \
    test("pddl-data/ipc-opt-noce/" P, testInit); \
} \
  \
TEST(testTransSystemMerge_##N) \
{ \
    test("pddl-data/ipc-opt-noce/" P, testMerge); \
}

TEST(testTransSystemInit_test)
{
    test("pddl-data/test-seq/test/pfile", testInit);
}

TEST(testTransSystemMerge_test)
{
    test("pddl-data/test-seq/test/pfile", testMerge);
}

TEST(testTransSystemInit_test_depot1)
{
    test("pddl-data/test-seq/depot/pfile1", testInit);
}

TEST(testTransSystemMerge_test_depot1)
{
    test("pddl-data/test-seq/depot/pfile1", testMerge);
}
#include "trans_system_prob.h"

