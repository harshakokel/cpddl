#ifndef TEST_TNF_H
#define TEST_TNF_H

TEST(testTNFSetUp);

#define P(N, P) \
    TEST(testTNFHeurEq_##N); \
    TEST(testDTNFHeurLT_##N);
#include "tnf_prob.h"

TEST_SUITE(TSTNF) {
    TEST_ADD(testTNFSetUp),
#define P(N, P) \
    TEST_ADD(testTNFHeurEq_##N), \
    TEST_ADD(testDTNFHeurLT_##N),
#include "tnf_prob.h"
    TEST_SUITE_CLOSURE
};
#endif

